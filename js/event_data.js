var eventname_desc = {
    coding: {
        name: "Coding",
        desc: "<li>Two Members per team are allowed.</li><li>The event will be conducted over two rounds.</li><li>The first round will be a written debugging and coding round.</li><li>The final round will be a live coding round with 5 questions.</li><li> Solutions must be coded in C or C++ in GCC or G++ compiler.</li><li>Time limit:<br/>a. First Round: 40 minutes.<br/>b. Final Round: 90 minutes.</li><li>The team to finish first with the most efficient code will be declared winner.</li><li>Any attempt to cheat will lead to disqualification.</li><li>Judges decision will be final.</li>",
        img: "coding.jpg",
        fee: "100"
    },
    it_quiz: {
        name: "IT Quiz",
        desc: "<li>Two Members per team are allowed.</li><li>The event will be conducted over two rounds.</li><li>The first round will be a written debugging and coding round.</li><li>The final round will be a live coding round with 5 questions.</li><li> Solutions must be coded in C or C++ in GCC or G++ compiler.</li><li>Time limit:<br/>a. First Round: 40 minutes.<br/>b. Final Round: 90 minutes.</li><li>The team to finish first with the most efficient code will be declared winner.</li><li>Any attempt to cheat will lead to disqualification.</li><li>Judges decision will be final.</li>",
        img: "it-quiz.jpg",
        fee: "100"
    },
    web_design: {
        name: "Web Designing",
        desc: "<li>Two Members per team are allowed.</li><li>The event will be conducted over two rounds.</li><li>The first round will be a written debugging and coding round.</li><li>The final round will be a live coding round with 5 questions.</li><li> Solutions must be coded in C or C++ in GCC or G++ compiler.</li><li>Time limit:<br/>a. First Round: 40 minutes.<br/>b. Final Round: 90 minutes.</li><li>The team to finish first with the most efficient code will be declared winner.</li><li>Any attempt to cheat will lead to disqualification.</li><li>Judges decision will be final.</li>",
        img: "web design.jpg",
        fee: "100"
    },
    debate: {
        name: "Debate",
        desc: "<li>Two Members per team are allowed.</li><li>The event will be conducted over 3 rounds.</li><li>The first round will be a written debugging and coding round.</li><li>The final round will be a live coding round with 5 questions.</li><li> Solutions must be coded in C or C++ in GCC or G++ compiler.</li><li>Time limit:<br/>a. First Round: 40 minutes.<br/>b. Final Round: 90 minutes.</li><li>The team to finish first with the most efficient code will be declared winner.</li><li>Any attempt to cheat will lead to disqualification.</li><li>Judges decision will be final.</li>",
        img: "debate.jpg",
        fee: "100"
    },
    et_quiz: {
        name: "Entertainment Quiz",
        desc: "<li>Two Members per team are allowed.</li><li>The event will be conducted over two rounds.</li><li>The first round will be a written debugging and coding round.</li><li>The final round will be a live coding round with 5 questions.</li><li> Solutions must be coded in C or C++ in GCC or G++ compiler.</li><li>Time limit:<br/>a. First Round: 40 minutes.<br/>b. Final Round: 90 minutes.</li><li>The team to finish first with the most efficient code will be declared winner.</li><li>Any attempt to cheat will lead to disqualification.</li><li>Judges decision will be final.</li>",
        img: "Et quiz.jpg",
        fee: "100"
    },
    photography: {
        name: "Photography",
        desc: "<li>Two Members per team are allowed.</li><li>The event will be conducted over two rounds.</li><li>The first round will be a written debugging and coding round.</li><li>The final round will be a live coding round with 5 questions.</li><li> Solutions must be coded in C or C++ in GCC or G++ compiler.</li><li>Time limit:<br/>a. First Round: 40 minutes.<br/>b. Final Round: 90 minutes.</li><li>The team to finish first with the most efficient code will be declared winner.</li><li>Any attempt to cheat will lead to disqualification.</li><li>Judges decision will be final.</li>",
        img: "Photography.jpg",
        fee: "150"
    },
    treasure_hunt: {
        name: "Treasure Hunt",
        desc: "<li>Two Members per team are allowed.</li><li>The event will be conducted over two rounds.</li><li>The first round will be a written debugging and coding round.</li><li>The final round will be a live coding round with 5 questions.</li><li> Solutions must be coded in C or C++ in GCC or G++ compiler.</li><li>Time limit:<br/>a. First Round: 40 minutes.<br/>b. Final Round: 90 minutes.</li><li>The team to finish first with the most efficient code will be declared winner.</li><li>Any attempt to cheat will lead to disqualification.</li><li>Judges decision will be final.</li>",
        img: "TreasureHunt.jpg",
        fee: "100"
    },
    movie_making: {
        name: "Movie Making",
        desc: "<li>Two Members per team are allowed.</li><li>The event will be conducted over two rounds.</li><li>The first round will be a written debugging and coding round.</li><li>The final round will be a live coding round with 5 questions.</li><li> Solutions must be coded in C or C++ in GCC or G++ compiler.</li><li>Time limit:<br/>a. First Round: 40 minutes.<br/>b. Final Round: 90 minutes.</li><li>The team to finish first with the most efficient code will be declared winner.</li><li>Any attempt to cheat will lead to disqualification.</li><li>Judges decision will be final.</li>",
        img: "movie-making.jpg",
        fee: "250"
    },
    namma_bhaashe_kannada: {
        name: "Namma Bhaashe Kannada",
        desc: "<li>Two Members per team are allowed.</li><li>The event will be conducted over two rounds.</li><li>The first round will be a written debugging and coding round.</li><li>The final round will be a live coding round with 5 questions.</li><li> Solutions must be coded in C or C++ in GCC or G++ compiler.</li><li>Time limit:<br/>a. First Round: 40 minutes.<br/>b. Final Round: 90 minutes.</li><li>The team to finish first with the most efficient code will be declared winner.</li><li>Any attempt to cheat will lead to disqualification.</li><li>Judges decision will be final.</li>",
        img: "kannadinga.jpg",
        fee: "100"
    },
    gaming: {
        name: "Gaming",
        desc: "<li>Two Members per team are allowed.</li><li>The event will be conducted over two rounds.</li><li>The first round will be a written debugging and coding round.</li><li>The final round will be a live coding round with 5 questions.</li><li> Solutions must be coded in C or C++ in GCC or G++ compiler.</li><li>Time limit:<br/>a. First Round: 40 minutes.<br/>b. Final Round: 90 minutes.</li><li>The team to finish first with the most efficient code will be declared winner.</li><li>Any attempt to cheat will lead to disqualification.</li><li>Judges decision will be final.</li>",
        img: "gaming.jpg",
        fee: "MM &amp; NFS :100, CS :300"
    },
    footloose: {
        name: "FootLoose",
        desc: "<li>Two Members per team are allowed.</li><li>The event will be conducted over two rounds.</li><li>The first round will be a written debugging and coding round.</li><li>The final round will be a live coding round with 5 questions.</li><li> Solutions must be coded in C or C++ in GCC or G++ compiler.</li><li>Time limit:<br/>a. First Round: 40 minutes.<br/>b. Final Round: 90 minutes.</li><li>The team to finish first with the most efficient code will be declared winner.</li><li>Any attempt to cheat will lead to disqualification.</li><li>Judges decision will be final.</li>",
        img: "footloose.jpg",
        fee: "500"
    },
    buzzinga: {
        name: "Buzzinga",
        desc: "<li>Two Members per team are allowed.</li><li>The event will be conducted over two rounds.</li><li>The first round will be a written debugging and coding round.</li><li>The final round will be a live coding round with 5 questions.</li><li> Solutions must be coded in C or C++ in GCC or G++ compiler.</li><li>Time limit:<br/>a. First Round: 40 minutes.<br/>b. Final Round: 90 minutes.</li><li>The team to finish first with the most efficient code will be declared winner.</li><li>Any attempt to cheat will lead to disqualification.</li><li>Judges decision will be final.</li>",
        img: "buzzinga.jpg",
        fee: "100"
    }
};
